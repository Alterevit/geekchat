import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientConnection implements Runnable {
    private Socket socket;
    private Scanner in;
    private PrintWriter out;

    public ClientConnection(Socket socket) {
        this.socket = socket;
        try {
            in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            out.println("echo: Hello user");
            while (true) {
                if (in.hasNext()) {

                    String str = new String(in.nextLine());
                    out.printf("echo:%s\n", str);
                    if (str.equals("exit")) {
                        out.println("Good by user");
                        break;
                    }
                }
            }
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
