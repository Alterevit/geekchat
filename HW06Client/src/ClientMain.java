public class ClientMain {
    public static View view;
    public static void main(String[] args) {
        view = new View();
        new Thread(new Controller()).start();
    }
}
