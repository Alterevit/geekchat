import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionListener implements Runnable {

    private ServerSocket serverSocket;
    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(8080);
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Connection Opened");
                new Thread(new ClientConnection(socket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

