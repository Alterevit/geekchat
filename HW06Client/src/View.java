import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class View extends JFrame {

    private JPanel pnlSend;
    private JPanel pnlChat;
    private JButton btnSend;
    private JTextField fldMessage;
    private JTextArea chatArea;
    private final int WIDTH = 400;
    private final int HEIGHT = 300;

    public View() {
        super("GeekChat");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        createChatArea();
        createSendPanel();

        btnSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!fldMessage.getText().isEmpty()) {
                    chatArea.append("User:" + fldMessage.getText() + "\n");
                    Controller.getOut().println(fldMessage.getText());
                    fldMessage.setText(null);
                }
            }
        });

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                    try {
                        Controller.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
            }
        });

        setVisible(true);
    }

    private void createChatArea() {
        pnlChat = new JPanel(new BorderLayout());
        pnlChat.setBorder(new EmptyBorder(1, 1, 1, 1));
        chatArea = new JTextArea();
        chatArea.setEditable(false);
        chatArea.setBorder(new EmptyBorder(1, 1, 1, 1));
        pnlChat.add(chatArea, BorderLayout.CENTER);
        add(pnlChat, BorderLayout.CENTER);
    }

    private void createSendPanel() {
        pnlSend = new JPanel(new BorderLayout());
        pnlSend.setBorder(new EmptyBorder(1, 1, 1, 1));
        btnSend = new JButton("Send");
        fldMessage = new JTextField();
        pnlSend.add(fldMessage, BorderLayout.CENTER);
        pnlSend.add(btnSend, BorderLayout.EAST);
        add(pnlSend, BorderLayout.SOUTH);
    }

    public JTextArea getChatArea() {
        return chatArea;
    }

}
