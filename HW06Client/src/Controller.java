import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Controller implements Runnable {
    private static Socket socket = null;
    private static Scanner in;
    private static PrintWriter out;

    @Override
    public void run() {
        try {
            socket = new Socket("localhost", 8080);
            in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
            ClientMain.view.setVisible(false);
            ClientMain.view.dispose();
        }
        while (true) {
            if (in.hasNext()) {
                ClientMain.view.getChatArea().append(in.nextLine() + "\n");
            }
        }
    }

    public static PrintWriter getOut() {
        return out;
    }

    public static void close() throws IOException {
        socket.close();
    }

    public static Boolean isConnected() {
        return socket.isConnected();
    }

}
